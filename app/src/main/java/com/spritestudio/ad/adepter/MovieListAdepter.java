package com.spritestudio.ad.adepter;

import android.content.Context;
import android.graphics.Movie;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spritestudio.ad.R;
import com.spritestudio.ad.model.MovieData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nuttapolwilailuk on 12/15/2016 AD.
 */

public class MovieListAdepter extends RecyclerView.Adapter<MovieListAdepter.ViewHolder> {


    private Context context;
    private static RecyclerViewClickListener itemListener;
    private CustomFilter mFilter;
    private List<MovieData> moveis;


    public MovieListAdepter(Context context, RecyclerViewClickListener itemListener, ArrayList<MovieData> movies) {
        this.context = context;
        this.itemListener=itemListener;
        this.moveis = movies;


        filteredList.addAll(moveis);

        mFilter = new CustomFilter(MovieListAdepter.this);

    }

    public MovieData getItemData(int position)
    {
        return filteredList.get(position);
    }

    public CustomFilter getFilter() {
        return mFilter;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        @BindView(R.id.imageView)
        ImageView cover_iv;

        @BindView(R.id.textView)
        TextView movie_title_tv;

        @BindView(R.id.downloadIcon)
        ImageView downloaded_icon_iv;

        private Context c;

        public Boolean fileExist = false;
        private MovieData md;

        public View v;
        public ViewHolder(View v ,Context c) {
            super(v);
            ButterKnife.bind(this,v);
            v.setOnClickListener(this);
            this.v = v;
            this.c = c;

            downloaded_icon_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(fileExist)
                        itemListener.deleteADByKey(md.ad_file_id);
                    else
                        itemListener.downloadADByKey(md.ad_file_id);

                    refreshStatus();
                }
            });
        }

        public void refreshStatus()
        {
            File f = new File(this.c.getExternalCacheDir().toString()+"/ad_"+this.md.ad_file_id+".mp3");

            fileExist = f.exists();
            if(fileExist) {
                downloaded_icon_iv.setContentDescription("ลบ AD");
                downloaded_icon_iv.setImageResource(R.mipmap.ic_delete_white_24dp);
            }else {
                downloaded_icon_iv.setContentDescription("ดาวน์โหลด AD");
                downloaded_icon_iv.setImageResource(R.mipmap.ic_file_download_white_24dp);
            }
        }

        public void setData(MovieData movieData) {

            Glide.with(this.c).load(movieData.cover).into(cover_iv);
//            cover_iv.setImageResource(R.drawable.cover);
            movie_title_tv.setText(movieData.ad_name);

            this.md = movieData;

            refreshStatus();
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v,this.getLayoutPosition());
        }
    }

    public interface RecyclerViewClickListener
    {
        public void recyclerViewListClicked(View v, int position);
        public void downloadADByKey(String key);
        public void deleteADByKey(String key);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_row_layout, parent, false);


        ViewHolder vh = new ViewHolder(v,this.context);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setData(filteredList.get(position));
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }


    private List<MovieData> filteredList = new ArrayList<MovieData>();

    public class CustomFilter extends Filter {
        private MovieListAdepter mAdapter;
        private CustomFilter(MovieListAdepter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filteredList.addAll(moveis);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final MovieData name : moveis) {
                    if (name.ad_name.toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(name);
                    }
                }
            }
            System.out.println("Count Number " + filteredList.size());
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            System.out.println("Count Number 2 " + ((List<String>) results.values).size());

            this.mAdapter.notifyDataSetChanged();
        }
    }
}


