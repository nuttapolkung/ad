package com.spritestudio.ad;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acrcloud.rec.sdk.ACRCloudClient;
import com.acrcloud.rec.sdk.ACRCloudConfig;
import com.acrcloud.rec.sdk.IACRCloudListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.spritestudio.ad.model.MovieData;
import com.spritestudio.ad.view.MoviePlayerFragment;
import com.spritestudio.ad.view.SelectMovieFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class HomeActivity extends AppCompatActivity implements IACRCloudListener {

    private ACRCloudClient mClient;
    private ACRCloudConfig mConfig;

    private String path = "";
    public MovieData curentMD;

    private boolean mProcessing = false;
    private boolean initState = false;

    private long startTime = 0;
    private long stopTime = 0;

    public static final String SYNC_STATE_FOUND="sync_found";
    public static final String SYNC_STATE_NOT_FOUND="sync_not_found";
    public static final String SYNC_STATE_ERROR="sync_error";


    private MediaPlayer mPlayer;

    private Timer timer = new Timer() ;
    private Boolean isSyncing = false;

    public VoiceListener getVoiceListener() {
        return voiceListener;
    }

    public void setVoiceListener(VoiceListener voiceListener) {
        this.voiceListener = voiceListener;
    }

    public interface SyncListener{
        public void onSync(String status);
        public void updatePlayerTime(long time);

    }

    public interface VoiceListener{
        public void onSpeechToText(String text);
    }

    public void setSyncListener(SyncListener syncListener) {
        this.syncListener = syncListener;
    }

   private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private VoiceListener voiceListener;
    private SyncListener syncListener;
    private TextToSpeech tts;
    private Boolean canSpeak = false;

    private Boolean isPlayList = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_home);


        ButterKnife.bind(this);

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {

                    int result = tts.setLanguage(new Locale("th"));


                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        canSpeak = true;
                    }
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });



        addPlayerList();


//        FirebaseStorage storage = FirebaseStorage.getInstance();


//        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//        mFirebaseRemoteConfig.setDefaults(R.xml.default_config);
//
//        mFirebaseRemoteConfig.activateFetched();
//
//        mFirebaseRemoteConfig.fetch(1000)
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.d("AD", "Fetch Succeeded");
//                            // Once the config is successfully fetched it must be activated before newly fetched
//                            // values are returned.
//                            mFirebaseRemoteConfig.activateFetched();
//                        } else {
//                            Log.d("AD", "Fetch failed");
//                        }
//                    }
//                });

        initAcrcloud();

//        CHeck permission
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO,  Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void speak(String message)
    {
        if(canSpeak)
            tts.speak(message,TextToSpeech.QUEUE_FLUSH,null);
    }

    public void addPlayerList()
    {
        SelectMovieFragment playlist = new SelectMovieFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,playlist);
        ft.commit();

        isPlayList = true;
    }

    public void addADPlayerFragment(MovieData md)
    {
        isPlayList = false;

        curentMD = md;

        MoviePlayerFragment moviePlayerFragment = new MoviePlayerFragment();
        moviePlayerFragment.md = md;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,moviePlayerFragment);
//        ft.addToBackStack("movie_player");
        ft.commit();

        int count  = getSupportFragmentManager().getBackStackEntryCount();
        Log.d("AD",count+"");

    }

    @Override
    public void onBackPressed() {
        if(!isPlayList)
            addPlayerList();
        else

        super.onBackPressed();
    }

    private void initAcrcloud()
    {
        path = Environment.getExternalStorageDirectory().toString()
                + "/acrclouds/model";

        File file = new File(path);
        if(!file.exists()){
            file.mkdirs();
        }

        this.mConfig = new ACRCloudConfig();
        this.mConfig.acrcloudListener = this;
        this.mConfig.context = this;
        this.mConfig.host = "identify-ap-southeast-1.acrcloud.com";//mFirebaseRemoteConfig.getString("host");
        this.mConfig.dbPath = path; // offline db path, you can change it with other path which this app can access.
        this.mConfig.accessKey ="3add92ceb88695bb628555e2bbe14c96";// "ba1b3dc8455c71c2323039e44c0073f9";//mFirebaseRemoteConfig.getString("accessKey");//"06ebdec4cbfc2c9799e5ac9dea071e7a";
        this.mConfig.accessSecret ="LPrxbgNhmgSvzcHwS032Q6JL4FfZVjkcGSYwLM0u";//"RyH35gZhJsEkOaYGCeXtXJUgyF8bY2VKib9T2QBD";//mFirebaseRemoteConfig.getString("accessSecret");// "jBnx1G1R3ipfeC4j2fleYKipzE2oDxjoTJMy3gna";
        this.mConfig.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_REMOTE;
//        this.mConfig.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_LOCAL;
//        this.mConfig.reqMode = ACRCloudConfig.ACRCloudRecMode.REC_MODE_BOTH;

        this.mClient = new ACRCloudClient();
        // If reqMode is REC_MODE_LOCAL or REC_MODE_BOTH,
        // the function initWithConfig is used to load offline db, and it may cost long time.
        this.initState = this.mClient.initWithConfig(this.mConfig);
        if (this.initState) {
            this.mClient.startPreRecord(3000); //start prerecord, you can call "this.mClient.stopPreRecord()" to stop prerecord.
        }

    }
//    Action
    public void syncMovie()
    {
        isSyncing=false;
        start();
    }

    public void playMovie()
    {
        if(mPlayer!=null)
            mPlayer.start();

        stop();
    }

    public void stopMovie()
    {
        if(mPlayer!=null)
            mPlayer.stop();

        stop();

        isSyncing = false;
    }


    public void start() {

        if (!this.initState) {
            Toast.makeText(this, "init error", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!isSyncing)
        if (!mProcessing) {
            mProcessing = true;
//            mPlayer.stop();
            if (this.mClient == null || !this.mClient.startRecognize()) {
                mProcessing = false;
            }
            startTime = System.currentTimeMillis();
        }
    }

    protected void stop() {

//        textView.setText("Stop");
        if (mProcessing && this.mClient != null) {
            this.mClient.stopRecordToRecognize();
        }
        mProcessing = false;

        timer.cancel();

        stopTime = System.currentTimeMillis();
    }

    protected void cancel() {
        if (mProcessing && this.mClient != null) {
            mProcessing = false;
            this.mClient.cancel();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.d("AD",result.get(0));
                }
                break;
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResult(String result) {
        if (this.mClient != null) {
            this.mClient.cancel();
            mProcessing = false;
        }

        String tres = "\n";

        // TODO: Use your own attributes to track content views in your app
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("Recognize")
                .putContentType("Music")
                .putContentId("1234")
                .putCustomAttribute("time",new Date().getTime())
                .putCustomAttribute("result", result));



        try {
            JSONObject j = new JSONObject(result);
            JSONObject j1 = j.getJSONObject("status");
            int j2 = j1.getInt("code");
            if(j2 == 0){
                JSONObject metadata = j.getJSONObject("metadata");
                //
                if (metadata.has("humming")) {
                    JSONArray hummings = metadata.getJSONArray("humming");
                    for(int i=0; i<hummings.length(); i++) {
                        JSONObject tt = (JSONObject) hummings.get(i);
                        String title = tt.getString("title");
                        JSONArray artistt = tt.getJSONArray("artists");
                        JSONObject art = (JSONObject) artistt.get(0);
                        String artist = art.getString("name");
                        tres = tres + (i+1) + ".  " + title + "\n";
                    }
                }
                if (metadata.has("music")) {
                    JSONArray musics = metadata.getJSONArray("music");
                    for(int i=0; i<musics.length(); i++) {
                        JSONObject tt = (JSONObject) musics.get(i);
                        String title = tt.getString("title");
                        JSONArray artistt = tt.getJSONArray("artists");
                        JSONObject art = (JSONObject) artistt.get(0);
                        String artist = art.getString("name");
                        tres = tres + (i+1) + ".  Title: " + title + "    Artist: " + artist + "\n";
                    }
                }
                if (metadata.has("streams")) {
                    JSONArray musics = metadata.getJSONArray("streams");
                    for(int i=0; i<musics.length(); i++) {
                        JSONObject tt = (JSONObject) musics.get(i);
                        String title = tt.getString("title");
                        String channelId = tt.getString("channel_id");
                        tres = tres + (i+1) + ".  Title: " + title + "    Channel Id: " + channelId + "\n";
                    }
                }
                if (metadata.has("custom_files")) {
                    JSONArray musics = metadata.getJSONArray("custom_files");
                    for(int i=0; i<musics.length(); i++) {
                        JSONObject tt = (JSONObject) musics.get(i);
                        String title = tt.getString("title");
                        if(mPlayer!=null)
                            mPlayer.stop();

                        String  urls = this.getExternalCacheDir().toString()+"/ad_"+this.curentMD.ad_file_id+".mp3";
                        Uri uri = Uri.parse(urls);
                        mPlayer = MediaPlayer.create(this,uri);

                        int time = tt.getInt("play_offset_ms")+1000;
                        if(time<0)
                            time*=-1;
                        mPlayer.seekTo(time);
                        mPlayer.start();

                        isSyncing = true;

                        tres = tres + (i+1) + ".  Title: " + title + "\n";

                        stop();

                        if(timer != null)
                            timer.cancel();

                        timer = new Timer();
                        timer.scheduleAtFixedRate(new TimerTask() {
                            @Override
                            public void run() {
                                if(syncListener != null && mPlayer != null)
                                syncListener.updatePlayerTime(mPlayer.getCurrentPosition());
                            }
                        },0,1000);

                        syncListener.onSync(SYNC_STATE_FOUND);


//                        textView.setText("Found");
                        Log.d("AD detect",tres);
                    }
                }
                tres = tres + "\n\n" + result;
            }else{
//                textView.setText("Not Found");
                syncListener.onSync(SYNC_STATE_NOT_FOUND);
                tres = result;
            }
        } catch (JSONException e) {
            tres = result;
            e.printStackTrace();

            syncListener.onSync(SYNC_STATE_ERROR);
        }

//        mResult.setText(tres);
    }

    @Override
    public void onVolumeChanged(double volume) {
//        long time = (System.currentTimeMillis() - startTime) / 1000;
//        mVolume.setText(getResources().getString(R.string.volume) + volume + "\n\n录音时间：" + time + " s");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("MainActivity", "release");
        if (this.mClient != null) {
            this.mClient.release();
            this.initState = false;
            this.mClient = null;
        }
    }
}
