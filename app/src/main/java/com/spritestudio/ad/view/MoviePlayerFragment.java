package com.spritestudio.ad.view;


import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spritestudio.ad.HomeActivity;
import com.spritestudio.ad.R;
import com.spritestudio.ad.model.MovieData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviePlayerFragment extends Fragment {


    public MoviePlayerFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.status_tv)
    TextView status_tv;

    @BindView(R.id.cover_iv)
    ImageView cover_iv;

    @BindView(R.id.sync_icon_iv)
    ImageView sync_icon_iv;

    @BindView(R.id.movie_title_tv)
    TextView movie_title_tv;

    @BindView(R.id.time_tv)
    TextView time_tv;

    @BindView(R.id.sync_fl)
    FrameLayout sync_fl;

    @BindView(R.id.play_fl)
    FrameLayout play_fl;

    @BindView(R.id.stop_fl)
    FrameLayout stop_fl;

    @BindView(R.id.controller_ll)
    LinearLayout controller_ll;

    public MovieData md;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movie_player, container, false);


        ButterKnife.bind(this,v);




        return v;
    }



    @OnClick(R.id.sync_fl)
    public void onClickSync()
    {
        this.setButtonStatus(STATE_SYNCING);
        this.setIsViewSync(true);

        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.syncMovie();
    }

    @OnClick(R.id.stop_fl)
    public void onClickStop()
    {
        this.setButtonStatus(STATE_STOP);
        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.stopMovie();
    }

    @OnClick(R.id.play_fl)
    public void onClickPlay()
    {
        this.setButtonStatus(STATE_PLAY);
    }


    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    private Date  date= new Date();
    @Override
    public void onStart() {
        super.onStart();

        Glide.with(this.getActivity()).load(md.cover).into(cover_iv);



        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.setSyncListener(new HomeActivity.SyncListener() {
            @Override
            public void onSync(String status) {
                switch (status)
                {
                    case HomeActivity.SYNC_STATE_FOUND:
                        setButtonStatus(STATE_PLAY);
                        setIsViewSync(false);
                        break;
                    case HomeActivity.SYNC_STATE_ERROR:
                        setButtonStatus(STATE_NOT_FOUND);
                        setIsViewSync(false);
                        break;
                    case HomeActivity.SYNC_STATE_NOT_FOUND:
                        setButtonStatus(STATE_NOT_FOUND);
                        setIsViewSync(false);
                        break;
                }
            }

            @Override
            public void updatePlayerTime(final long time) {

                date.setTime(time);
                if(getActivity()!=null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String timeString =  String.format(Locale.getDefault(),"%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(time),
                                TimeUnit.MILLISECONDS.toMinutes(time) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)), // The change is in this line
                                TimeUnit.MILLISECONDS.toSeconds(time) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
                        time_tv.setText(timeString);
                    }
                });

            }
        });
        if(!isFirstLoad)
        {
            isFirstLoad = true;
            onClickSync();
        }

//demo
//        startSync();
    }

    private Boolean isFirstLoad = false;

    public void startSync()
    {
        this.setButtonStatus(STATE_SYNCING);
        this.setIsViewSync(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setIsViewSync(false);
                setButtonStatus(STATE_PLAY);
            }
        },2000);
    }

    private  final String MESSAGE_SYNCING = "กำลังค้นหา AD";
    private  final String MESSAGE_PLAYIN = "กำลังเล่น";
    private  final String MESSAGE_STOP = "หยุดเล่น";
    private  final String MESSAGE_NOT_FOUND = "ไม่พบ AD";

    private  final String STATE_SYNCING = "syncing";
    private  final String STATE_STOP = "stop";
    private  final String STATE_PLAY = "play";
    private  final String STATE_NOT_FOUND = "not_found";

    private void setIsViewSync(Boolean sync)
    {
        if(sync){

            sync_icon_iv.setVisibility(View.VISIBLE);
            controller_ll.setVisibility(View.INVISIBLE);
            time_tv.setVisibility(View.INVISIBLE);
            cover_iv.setColorFilter(Color.argb(155,0,0,0));
        }else{
            sync_icon_iv.setVisibility(View.GONE);
            controller_ll.setVisibility(View.VISIBLE);
            time_tv.setVisibility(View.VISIBLE);
            cover_iv.setColorFilter(Color.TRANSPARENT);
            cover_iv.clearColorFilter();
        }
    }

    private void setButtonStatus(String state)
    {
        sync_fl.setVisibility(View.GONE);
        play_fl.setVisibility(View.GONE);
        stop_fl.setVisibility(View.GONE);

        if(state.equals(STATE_PLAY))
        {
            status_tv.setText(MESSAGE_PLAYIN);
            stop_fl.setVisibility(View.VISIBLE);
        }else if(state.equals(STATE_STOP))
        {
            status_tv.setText(MESSAGE_STOP);
            sync_fl.setVisibility(View.VISIBLE);
//            play_fl.setVisibility(View.VISIBLE);
        }else if(state.equals(STATE_NOT_FOUND))
        {
            status_tv.setText(MESSAGE_NOT_FOUND);
            sync_fl.setVisibility(View.VISIBLE);
        }else if(state.equals(STATE_SYNCING))
        {
            status_tv.setText(MESSAGE_SYNCING);
        }

        status_tv.setContentDescription(status_tv.getText().toString());
        HomeActivity homeActivity = (HomeActivity) getActivity();

        if(homeActivity!=null)
        homeActivity.speak(status_tv.getText().toString());
    }
}
