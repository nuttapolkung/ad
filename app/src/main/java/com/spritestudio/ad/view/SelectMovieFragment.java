package com.spritestudio.ad.view;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.spritestudio.ad.HomeActivity;
import com.spritestudio.ad.R;
import com.spritestudio.ad.adepter.MovieListAdepter;
import com.spritestudio.ad.model.MovieData;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectMovieFragment extends Fragment implements HomeActivity.VoiceListener {


    @BindView(R.id.recycleview)
    RecyclerView recyclerView;

    @BindView(R.id.search_et)
    EditText search_et;

    @BindView(R.id.mic_ib)
    ImageButton mic_ib;

    @OnClick(R.id.mic_ib)
    public void callSpeechToText()
    {
        /**
         * Showing google speech input dialog
         * */

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(2000));
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"พูดชื่อหนังที่ต้องการค้นหา");


        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Log.d("AD",a.toString());
        }

    }

    private ThinDownloadManager dlm = new ThinDownloadManager();
    private ArrayList<MovieData> movies = new ArrayList<>();
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_movie, container, false);
        ButterKnife.bind(this,view);

        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.setVoiceListener(this);

        initContent();
        return view;
    }

    private MovieListAdepter mAdapter;

    @Override
    public void onDestroyView() {

        ViewGroup vg = (ViewGroup) getView();
        vg.removeAllViews();

        super.onDestroyView();
    }

    private Boolean checkDownload(String url, String key)
    {
        Uri downloadUri = Uri.parse(url);
        String destString = this.getActivity().getExternalCacheDir().toString()+"ad_"+key+".mp3";
        Uri destinationUri = Uri.parse(destString);


        File f = new File(destString);

        if(!f.exists())
        {

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadContext(this)//Optional
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int id) {

                    }

                    @Override
                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {

                    }

                    @Override
                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress) {

                    }
                });

        dlm.add(downloadRequest);

            return false;
        }else{
            return true;
        }
    }

    private Promise loadAD(String key)
    {
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference storageRef = storage.getReferenceFromUrl("gs://pannana-3c22c.appspot.com");

        StorageReference ref = storageRef.child(key);

        final Deferred deferred = new DeferredObject();

        File rootPath = new File(Environment.getExternalStorageDirectory(), "ad");
        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

//        final File localFile = new File(getActivity().getExternalCacheDir(),rootPath,key+".mp3");
        final File localFile;
//        try {
            localFile = new File(getActivity().getExternalCacheDir(),"ad_"+key+".mp3");
//            localFile = File.createTempFile("ad_", ".mp3", getActivity().getExternalCacheDir());


            ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Log.e("firebase ",";local tem file created  created " +localFile.toString());
                    //  updateDb(timestamp,localFile.toString(),position);

                    Log.d("AD", taskSnapshot.toString());
                    mAdapter.getFilter().filter("");

                    deferred.resolve(taskSnapshot);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e("firebase ",";local tem file not created  created " +exception.toString());
                }
            });
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



        return deferred.promise();
    }

    private void initContent() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("movies");
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                for (DataSnapshot movieData: dataSnapshot.getChildren()) {
                    MovieData data = dataSnapshot.getValue(MovieData.class);
                    Log.d("AD",dataSnapshot.toString());

                    if(data.enable) {
//                        loadAD(data.ad_file_id);
//                        checkDownload(data.ad_file_id,data.ad_id);
                        movies.add(data);
                    }

                mAdapter.notifyDataSetChanged();
                mAdapter.getFilter().filter("");
//                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {



                MovieData data = dataSnapshot.getValue(MovieData.class);


//                checkDownload(data.ad_file_id,data.ad_id);
                int find =-1;
                for (int i = 0; i < movies.size(); i++) {
                    if(movies.get(i).ad_id.equals(data.ad_id))
                    {
                        find = i;
                        break;
                    }
                }

                if(find!=-1)
                {
                    movies.get(find).enable = data.enable;
                }

                if(data.enable) {
                    loadAD(data.ad_file_id);
                    movies.add(data);
                }else if(find!=-1)
                {
                    movies.remove(find);
                }

                mAdapter.getFilter().filter("");

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                MovieData data = dataSnapshot.getValue(MovieData.class);
                int find =-1;
                for (int i = 0; i < movies.size(); i++) {
                    if(movies.get(i).ad_id.equals(data.ad_id))
                    {
                        find = i;
                        break;
                    }
                }
                if(find!=-1)
                {
                    movies.remove(find);

                    mAdapter.getFilter().filter("");
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        mAdapter = new MovieListAdepter(getContext(), new MovieListAdepter.RecyclerViewClickListener() {
            @Override
            public void downloadADByKey(String key) {
                startLoadAD(key);
            }

            @Override
            public void deleteADByKey(String key) {
                deleteADFromKey(key);
            }

            @Override
            public void recyclerViewListClicked(View v, int position) {

               final MovieData md = mAdapter.getItemData(position);

                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                if(checkADFileEXitsFromKey(md.ad_file_id))
                {

                    HomeActivity homeActivity = (HomeActivity) getActivity();
                    homeActivity.addADPlayerFragment(md);
                }else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Pannana")
                            .setContentText("กรุณาโหลดไฟล์AD ก่อนใช้งาน")
                            .setConfirmText("ดาว์โหลดเลย")
                            .setCancelText("ยกเลิก")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                    startLoadAD(md.ad_file_id);

                                }
                            })
                            .show();
                }
            }
        },movies);



        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new CenterScrollListener());

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void startLoadAD(String file_id)
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Downloading..");
        pDialog.setCancelable(false);
        pDialog.show();

        loadAD(file_id).then(new DoneCallback() {
            @Override
            public void onDone(Object result) {
                pDialog.dismiss();
            }
        });
    }

    private  Boolean checkADFileEXitsFromKey(String key)
    {
        return checkFilesExits(this.getActivity().getExternalCacheDir().toString()+"/ad_"+key+".mp3");
    }

    private Boolean checkFilesExits(String path)
    {
        File f = new File(path);

        return f.exists();
    }

    private void deleteADFromKey(String key)
    {
        File f = new File(this.getActivity().getExternalCacheDir().toString()+"/ad_"+key+".mp3");
        f.delete();
    }

    @Override
    public void onSpeechToText(String text) {
        search_et.setText(text);
    }
}
