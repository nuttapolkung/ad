package com.spritestudio.ad.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by pir2developermacpro on 2/6/2017 AD.
 */
@IgnoreExtraProperties
public class MovieData {

    public String ad_file_id;
    public String ad_id;
    public String ad_name;
    public String cover;
    public Boolean enable;


    public MovieData()
    {

    }
}
